# =================
# bootstrap-console
# =================

FROM docker.io/library/archlinux:latest as bootstrap-console
ENTRYPOINT [ "/usr/sbin/init" ]
CMD [ "--log-level=info", "--unit=multi-user.target" ]

USER root
WORKDIR /root

# configure archlinux image
RUN --mount=type=bind,src=archlinux-scriptbase/prepare-podman.sh,dst=/root/prepare-podman.sh /root/prepare-podman.sh

# configure bootstrap
RUN --mount=type=bind,src=archlinux-scriptbase/bootstrap,dst=/root/bootstrap /root/bootstrap/configure-bootstrap.sh

# ===================
# localmirror-console
# ===================

FROM localhost/archlinux-bootstrap-console:latest as localmirror-console

# configure localmirror
RUN --mount=type=bind,src=archlinux-scriptbase/localmirror,dst=/root/localmirror \
    /root/localmirror/configure-localmirror.sh

# ================
# database-console
# ================

FROM localhost/archlinux-bootstrap-console:latest as database-console

# configure database
RUN --mount=type=bind,src=archlinux-scriptbase/database,dst=/root/database \
    /root/database/configure-database.sh

# ================
# router-console
# ================

FROM localhost/archlinux-bootstrap-console:latest as router-console

# configure router
RUN --mount=type=bind,src=archlinux-scriptbase/router,dst=/root/router \
    /root/router/configure-router.sh

# ================
# router-console
# ================

FROM localhost/archlinux-bootstrap-console:latest as graphical-desktop

# configure router
RUN --mount=type=bind,src=archlinux-scriptbase/graphical,dst=/root/graphical \
    /root/graphical/configure-graphical.sh
