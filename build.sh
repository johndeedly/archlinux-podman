#!/usr/bin/env bash
set -u -o pipefail -o errtrace +o history
set_err_trap() {
  trap 's=$?; echo "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR
}
unset_err_trap() {
  trap - ERR
}
set_err_trap

USER="$(id -un)"
if [ "$EUID" -eq 0 ]; then
  echo >&2 "Please run the script as non root user"
  exit 1
else
  echo "Running as current user '$USER'"
fi

./archlinux-scriptbase/prepare-buildah.sh -n archlinux-bootstrap
./archlinux-scriptbase/install-scriptbase.sh -w "$(pwd)/archlinux-scriptbase" -s bootstrap
./archlinux-scriptbase/finalize-buildah.sh -n archlinux-bootstrap

# ./archlinux-scriptbase/prepare-buildah.sh -n archlinux-localmirror
# ./archlinux-scriptbase/install-scriptbase.sh -w "$(pwd)/archlinux-scriptbase" -s bootstrap -s localmirror
# ./archlinux-scriptbase/finalize-buildah.sh -n archlinux-localmirror

# ./archlinux-scriptbase/prepare-buildah.sh -n archlinux-database
# ./archlinux-scriptbase/install-scriptbase.sh -w "$(pwd)/archlinux-scriptbase" -s bootstrap -s database
# ./archlinux-scriptbase/finalize-buildah.sh -n archlinux-database

# ./archlinux-scriptbase/prepare-buildah.sh -n archlinux-graphical
# ./archlinux-scriptbase/install-scriptbase.sh -w "$(pwd)/archlinux-scriptbase" -s bootstrap -s graphical
# ./archlinux-scriptbase/finalize-buildah.sh -n archlinux-graphical

podman image prune -f
