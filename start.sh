#!/usr/bin/env bash
set -u -o pipefail -o errtrace +o history
set_err_trap() {
  trap 's=$?; echo "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR
}
unset_err_trap() {
  trap - ERR
}
set_err_trap

USER="$(id -un)"
if [ "$EUID" -eq 0 ]; then
  echo >&2 "Please run the script as non root user"
  exit 1
else
  echo "Running as current user '$USER'"
fi

podman run --name archlinux-bootstrap-console --rm --hostname bootstrap --systemd=true --tty --detach --cap-add=SYS_CHROOT,NET_ADMIN,NET_RAW archlinux-bootstrap-console
