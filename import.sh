#!/usr/bin/env bash
set -u -o pipefail -o errtrace +o history
set_err_trap() {
  trap 's=$?; echo "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR
}
unset_err_trap() {
  trap - ERR
}
set_err_trap

USER="$(id -un)"
if [ "$EUID" -eq 0 ]; then
  echo >&2 "Please run the script as non root user"
  exit 1
else
  echo "Running as current user '$USER'"
fi

import_tar() {
  with="$1"
  without="${with%.tar}"
  if [ -f "$with" ]; then
    podman image rm "$without"
    podman load -i $with
  fi
}

import_tar archlinux-bootstrap-console.tar
# import_tar archlinux-localmirror-console.tar
# import_tar archlinux-database-console.tar
# import_tar archlinux-router-console.tar
