#!/usr/bin/env bash
set -u -o pipefail -o errtrace +o history
set_err_trap() {
  trap 's=$?; echo "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR
}
unset_err_trap() {
  trap - ERR
}
set_err_trap

USER="$(id -un)"
if [ "$EUID" -eq 0 ]; then
  echo >&2 "Please run the script as non root user"
  exit 1
else
  echo "Running as current user '$USER'"
fi

doas /usr/bin/env bash <<EOF
  doas usermod --add-subuids 10000-75535 ${USER}
  doas usermod --add-subgids 10000-75535 ${USER}
EOF
podman system migrate
