#!/usr/bin/env bash
set -u -o pipefail -o errtrace +o history
set_err_trap() {
  trap 's=$?; echo "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR
}
unset_err_trap() {
  trap - ERR
}
set_err_trap

USER="$(id -un)"
if [ "$EUID" -eq 0 ]; then
  echo >&2 "Please run the script as non root user"
  exit 1
else
  echo "Running as current user '$USER'"
fi

HOSTNAME="bootstrap"
CONTAINERNAME="archlinux-${HOSTNAME}"

if ! podman inspect "${CONTAINERNAME}" >/dev/null 2>&1; then
  if ! podman load -i "${CONTAINERNAME}.tar"; then
    1>&2 echo "The podman container ${CONTAINERNAME} was not found. Please run ./build.sh first."
    exit 1
  fi
fi

alacritty -e podman run --name "${CONTAINERNAME}" --hostname "${HOSTNAME}" \
  --systemd=true --tty -p 127.0.0.1:8022:22 \
  --cap-add=SYS_CHROOT,NET_ADMIN,NET_RAW "${CONTAINERNAME}" &

sleep 5

# - with ssh (don't forget port forwarding: -p 127.0.0.1:8022:22)
alacritty -e ssh -o "StrictHostKeyChecking no" -o "UserKnownHostsFile=/dev/null" -p 8022 user@127.0.0.1
# - with pseudo terminal
# alacritty -e podman exec --tty --interactive "${CONTAINERNAME}" login --
# - with xrdp connection (don't forget port forwarding: -p 127.0.0.1:33389:3389)
# alacritty -e xfreerdp /size:800x600 /v:127.0.0.1:33389

podman container stop "${CONTAINERNAME}"
podman container rm -f "${CONTAINERNAME}"
