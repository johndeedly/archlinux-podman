# Arch Linux Podman Bootstrap

<div align="center">

<a href="">[![CC BY-SA 4.0][cc-by-sa-shield]][cc-by-sa]</a>
<a href="">![project status][status-shield]</a>

</div>

This work is licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License][cc-by-sa].

In addition you agree to the attached [disclaimer][disclaimer].

[cc-by-sa]: LICENSE
[cc-by-sa-shield]: https://img.shields.io/badge/license-CC%20BY--SA%204.0-informational.svg
[disclaimer]: DISCLAIMER.md
[status-shield]: https://img.shields.io/badge/status-active%20development-brightgreen.svg

# Installation Process

```bash
# does not need root like other container engines
# podman ftw!!

# execute first to prepare uid and gid mapping for the current logged in user on the host machine. (needs sudo for that)
# doesn't explode if executed multiple times
$ ./prepare.sh
# build the image
$ ./build.sh
# optional: export and import the archlinux-systemd.tar (Docker export format)
$ ./export.sh
$ ./import.sh
# run the image
$ ./start.sh
# optional: debug the image (login as root inside the container)
$ ./debug.sh
```

You're welcome. Have fun! --johndeedly
